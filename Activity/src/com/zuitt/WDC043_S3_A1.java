package com.zuitt;

import java.util.Scanner;

public class WDC043_S3_A1 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int factorial = 1;


        int i = 1;

        System.out.println("Input an Integer whose factorial will be computed: ");

        try{
            int num = input.nextInt();

            while( i <= num ){
                factorial = factorial * i;
                i++;
            }

            System.out.println("The factorial of " +num+ " is "+ factorial);
        } catch (Exception e){
            System.out.println("Invalid Input! ");
            e.printStackTrace();
        }finally {
            System.out.println("default value" + factorial);
        }

//using for loop

        int factorial2 = 1;
        System.out.println("for loop with test for zero & negative outputs");
        System.out.println("Please enter a second number: ");
        try{
            int num2 = input.nextInt();


                if(num2 < 0){

                    System.out.println("Invalid Input! must be a non-zero or negative number ");

                }

                else{
                    for(int x = 1;x <= num2 ; x++) {
                        factorial2 = factorial2 * x;
                    }
                    System.out.println("The factorial of " +num2 + " using for loop is "+ factorial2);
                }





        } catch (Exception e){
            System.out.println("Invalid Input! ");
            e.printStackTrace();
        }finally {
            System.out.println("default value" + factorial);
        }





    }
}
