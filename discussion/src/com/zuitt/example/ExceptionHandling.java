package com.zuitt.example;

import java.util.Scanner;

public class ExceptionHandling {

    //Exceptions
    //problem that arises during the execution of a program
    //it disrupts the normal flow of the program and terminates it abnormally

    //Exception Handling
    //refers to managing and catching run-time errors in order to safely run your code

    //Errors encounter in Java
        //Compile-time errors - errors that usually happen when you try to compile a program that is syntactically incorrect or has missing package imports
        //there is an error and is not able to run
    //Run-time error - errors that happen after happen after the compilation and during the execution of the program
        //common example: user gives a String instead of number


    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        int num = 0;

        System.out.println("Please enter a number: ");
        //try is to execute the statement
        try{
            num = input.nextInt();
        }
        //catch - is to catch errors
        //Exception class represented by an e
        //the class Exception and its subclasses are a form of throwable that indicates conditions that a reasonable application might want to catch

        catch (Exception e){

            System.out.println("Invalid Input! ");

            //prints throwable error along with other details like the line number, class name where the exception occured

            e.printStackTrace();
        }
        // optional block - it will execute if there are any errors encountered or not
        //finally -useful when dealing with highly sensitive operations
        finally {
            System.out.println("I will run no matter what happens! you have entered: "+ num);
        }
        System.out.println("Hello");
    }

}
